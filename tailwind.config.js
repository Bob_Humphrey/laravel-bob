module.exports = {
  purge: {
    enabled: true,
    content: [
      './vendor/laravel/jetstream/**/*.blade.php',
      './storage/framework/views/*.php',
      './resources/views/**/*.blade.php',
      './resources/views/*.blade.php'
    ],
  },
  theme: {
    extend: {
      spacing: {
        "72": `18rem`
      },
    },
    colors: {
      "medium-gray": `#777C83`,
      "warm_gray-100": `#F5F5F4`,
      "warm_gray-200": `#E7E5E4`,
      
      "gray-50": `#FAFAFA`,
      "gray-100": `#F5F5F5`,
      "gray-200": `#EEEEEE`,
      "gray-300": `#E0E0E0`,
      "gray-400": `#BDBDBD`,
      "gray-500": `#9E9E9E`,
      "gray-600": `#757575`,
      "gray-700": `#616161`,
      "gray-800": `#424242`,
      "gray-900": `#212121`,

      "warm-gray-500": `#78716C`,
      "warm-gray-600": `#57534E`,

      "red-400": `#EF5350`,
      "red-700": `#D32F2F`,
      "purple-400": `#AB47BC`,
      "green-700": `#388E3C`,
      "yellow-400": `#FFEE58`,
      "yellow-700": `#FBC02D`,
      "orange-400": `#FFA726`,

      "tan": `#C5BDB2`,
      "dark-tan": `#8E7461`,
      "medium-gray": `#777C83`,
      "dark-gray": `#5F5A58`,
      "light-tan": `#F3ECE4`,

      "black": `#000000`,
      "white": `#FFFFFF`
    },

    fontFamily: {
      source: ['source'],
      roboto: ['roboto'],
      roboto_bold: ['roboto_bold'],
      nunito_black: [`nunito_black`],
      nunito_black_italic: [`nunito_black_italic`],
      nunito_bold: [`nunito_bold`],
      nunito_bold_italic: [`nunito_bold_italic`],
      nunito_extrabold: [`nunito_extrabold`],
      nunito_extrabold_italic: [`nunito_extrabold_italic`],
      nunito_extralight: [`nunito_extralight`],
      nunito_extralight_italic: [`nunito_extralight_italic`],
      nunito_italic: [`nunito_italic`],
      nunito_light: [`nunito_light`],
      nunito_light_italic: [`nunito_light_italic`],
      nunito_regular: [`nunito_regular`],
      nunito_semibold: [`nunito_semibold`],
      nunito_semibold_italic: [`nunito_semibold_italic`],
      serif: [
        `roboto_slabregular`,
        `Georgia`,
        `Cambria`,
        `"Times New Roman"`,
        `Times`,
        `serif`
      ],
      mono: [
        `Menlo`,
        `Monaco`,
        `Consolas`,
        `"Liberation Mono"`,
        `"Courier New"`,
        `monospace`
      ]
    }
  },
  variants: {
      // extend: {
      //  grayscale: ['hover', 'focus'],
      // }
    },
  plugins: [

  ],
}
