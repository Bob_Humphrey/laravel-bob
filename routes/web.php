<?php

use App\Http\Livewire\ContactForm;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('page-cache')->get('/', function () {
  return view('welcome');
});

Route::middleware('page-cache')->get('/experience', function () {
  return view('experience');
});

Route::middleware('page-cache')->get('/work', function () {
  return view('work');
});

Route::middleware('page-cache')->get('/randall-projects', function () {
  return view('randall');
});

Route::middleware('page-cache')->get('/skills', function () {
  return view('skills');
});

Route::middleware('page-cache')->get('/php', function () {
  return view('php');
});

Route::get('/contact', ContactForm::class);
