<div class="w-5/6 lg:w-2/3 mx-auto">

  <section class="">

    <div class="">

      {{-- FORM --}}

      <div class="col-span-1" x-data="{ showSubject: false }">

        <x-alert type="success" class="bg-green-700 text-white p-4 mb-4 rounded mx-auto" />
        <x-alert type="danger" class="bg-red-700 text-white p-4 mb-4 rounded mx-auto" />

        {{-- NAME --}}

        <div class="text-xs uppercase mb-4">
          <span class="">
            YOUR
          </span>
          <x-label for="name" />
          <span class="text-gray-400">
            (Required)
          </span>

          <x-input name="name" wire:model="name"
            class="p-2 bg-gray-50 text-base rounded border border-gray-200 w-full appearance-none mt-2" />

          <x-error field="name" class="text-red-500 text-xs mt-2">
            <ul>
              @foreach ($component->messages($errors) as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </x-error>
        </div>

        {{-- EMAIL --}}

        <div class="text-xs uppercase mb-2">
          <span class="">
            YOUR
          </span>
          <x-label for="email" />
          <span class="text-gray-400">
            (Required)
          </span>

          <x-input name="email" wire:model="email"
            class="bg-gray-50 text-base p-2 rounded border border-gray-200 w-full appearance-none mt-2" />

          <x-error field="email" class="text-red-500 text-xs mt-2">
            <ul>
              @foreach ($component->messages($errors) as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </x-error>
        </div>

        {{-- SUBJECT --}}

        <div class="text-xs uppercase mb-2" x-show="showSubject">
          <x-label for="subject" />
          <span class="text-gray-400">
            (Required)
          </span>

          <x-input name="subject" wire:model="subject"
            class="bg-gray-50 text-base p-2 rounded border border-gray-200 w-full appearance-none mt-2" />

          <x-error field="subject" class="text-red-500 text-xs mt-2">
            <ul>
              @foreach ($component->messages($errors) as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </x-error>
        </div>

        {{-- MESSAGE --}}

        <div class="text-xs uppercase mb-2">
          <x-label for="message" />
          <span class="text-gray-400">
            (Required)
          </span>

          <x-textarea name="message" rows="5" wire:model="message"
            class="bg-gray-50 text-base p-2 rounded border border-gray-200 w-full appearance-none mt-2" />

          <x-error field="message" class="text-red-500 text-xs mt-2">
            <ul>
              @foreach ($component->messages($errors) as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </x-error>
        </div>

        {{-- SPAM FILTER --}}

        <div class="text-xs uppercase mb-2">
          <span class="">
            NOT A ROBOT
          </span>
          <span class="text-gray-400">
            (Required)
          </span>

          <div class="mt-2">
            <span class="text-base">
              {{ $spamFilterFirstNumber }} {{ $operationSymbol }} {{ $spamFilterSecondNumber }} =
            </span>
            <x-input name="spam_filter_result" wire:model="spam_filter_result"
              class="bg-gray-50 text-base p-2 rounded border border-gray-200 w-24 appearance-none" />
          </div>

          <x-error field="spam_filter_result" class="text-red-500 text-xs mt-2">
            <ul>
              @foreach ($component->messages($errors) as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </x-error>
        </div>

        {{-- SUBMIT BUTTON --}}

        <div class="flex justify-center mt-4">
          <button type="submit" wire:click="sendMessage"
            class="bg-dark-tan hover:bg-black text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
            Send
          </button>
        </div>

      </div>

    </div>

  </section>

</div>
