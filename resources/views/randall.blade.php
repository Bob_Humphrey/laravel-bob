<x-layouts.app>

  <div class="w-full text-lg">

    <h2 class="w-full font-roboto_bold text-3xl text-black text-center mb-3">
      Randall Library Projects
    </h2>

    <div class="text-center mb-10">
      <a href={{ url('/work') }} class="font-nunito_bold text-dark-tan hover:text-black">
        View Personal Projects here
      </a>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Assignment Calculator
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="assignment-calculator" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        This application helps students manage their time and meet deadlines by breaking large projects into smaller
        steps and budgeting specific amounts of time for each step. This is an open source project that I customized for
        Randall Library.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Batch Monitor
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="batch-monitor" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        The library has a number of computer jobs running automatically without anyone attending them. This application
        monitors these jobs and provides a report showing which jobs are running. It also notifies responsible personnel
        when any of the jobs fails or quits running.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Bento Box Search
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="bento" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        An application to help students search for library resources, such as books, journals, magazines, and digital
        collections. Before this was built, they had to enter a separate search for each category of items.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Best Bets
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="best-bets" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        This is an enhancement to the library website search function that allows librarians to highlight particular
        materials in the collection.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Click Heat
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="clickheat" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Customized an open source application that creates a visual record of how the library's information kiosk is
        being used.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        CMC Maker Space
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="cmc-makerspace" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        The Curriculum Materials Center contains a collection of PreK-12 instructional resources, including a makerspace
        with stations for 3D doodling/printing/scanning/modeling, book binding, paper-crafting, stamping, painting,
        prototyping, and sewing. This application assists students and staff in documenting projects completed in the
        makerspace using descriptions, photos, and inventories of resources consumed.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        CMC Reference Transactions
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="cmc-reference-transactions" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Documents the various occurrences of customer service provided by the library's Curriculum Materials Center.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Computer Availability
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="computer-availability" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        There are over one hundred computers in the library that are available for student use. This application
        provides detailed reporting on how these computers are actually used, by date, time, device type, machine, and
        location. This information has a number of uses, including identifying malfunctioning machines and establishing
        justification for new computers as needed. The library's application was written in house to replace an
        expensive proprietary system.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Computers Available for Borrowing
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="borrow-from-tac" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        The library's Technology Assistance Center provides laptops that can be borrowed by students. This
        application creates a public display that shows the number of laptops currently available.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Dashboard
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="dashboard" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Graphical display of several categories of service provided by the library.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Database Ranking
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="eresource-ranking" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Enhanced the ability of the librarians to customize certain features in the library's content management system.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Drupal 7 Migration
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="drupal-7-migration" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Drupal is the library's open source content management system. I migrated 12 different websites from Drupal
        version 6 to Drupal version 7.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        E-books
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="ebooks" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Provides quick access to the library's Information Technology department collection of programming reference
        e-books.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Electronic Resource Tickets
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="eresource-tickets" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Ticketing app to manage problems with the library's electronic resources.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Facts and Planning
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="facts-and-planning" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        A public accounting of various services provided by Randall Library to the faculty and students of the
        university .
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Fund Report for Selectors
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="fund-report-for-selectors" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Each year the library establishes a budget by university department for purchasing new books. This application
        helps the library manage these budgets and purchases.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Get It from Randall Library
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="get-it-from-randall" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Research tool that can be easily added as a button to a student's browser bookmarks toolbar.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Government Docs Weeding
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="government-docs-weeding" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Randall Library participates in the Federal Depository Library Program to make government documents available
        for research and other uses. Over time, the number of these documents exceeds the space available in the
        library. This application helps library personnel identify items that can be removed from the collection.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Group Study Room Reservations
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="group-study-reservations" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        An application that allows students to reserve one of the library's private study rooms. With this software, the
        students are able to completely manage the use of the the various study rooms with almost no involvement from
        the library's staff.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Group Study Statistics
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="group-study-statistics" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Reporting showing usage of the group study reservation app.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Guide Duplicates
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="guide-duplicates" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Used by library admins to find duplicate resources.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Guide on the Side
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="guide-on-the-side" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Tool for creating web based training applications.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Guides
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="guides" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Guides are created for each area of study at the university. Students can use them to locate important library
        resources and contact the librarian responsible for that field.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Improved Interlibrary Loan
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="interlibrary-loan" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Allows library patrons to request materials from other libraries through Randall Library. This was vendor
        software that I customized and improved.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Info Lit
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="info-lit" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        A collection of tools for teaching and learning information literacy skills.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Instructions
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="instructions-statistics" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Documents all of the various classes taught by library faculty.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Item Bib Mismatch
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="item-bib-mismatch" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        An application that locates inconsistencies in the library catalog so that errors can be corrected.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Item Status Reports
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="item-status-reports" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        An enhancement to the library catalog that is used by the staff to find catalog items by means of various
        criteria.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Link Translator
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="link-translator" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Tool for updating the library's website links after a vendor's information was changed.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Linux Servers
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="linux" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Set up new Linux servers, installed all software, and migrated applications from the library's Windows servers.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Oral History PDFs
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="special-collections-pdf" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        The library has created several dozen oral histories based on interviews with various members of the Wilmington
        NC community. These interviews were already available online, but I created a function by which any history
        could be turned into a PDF document.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Overdue Search List
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="overdue-search-list" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Reporting on overdue collection items, including books, movies, and music.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Price List
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="price-list" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Various departments at the university are budgeted annual funds for purchase of new library materials. This
        application helps the library staff to track and manage these budgets as new items are acquired.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Project Management
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="project-management" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        The library information technology department was using the Asana web application for managing various projects.
        This application was able to access the Asana server and create customized reports by project and employee.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Public Help Desk
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="public-help-desk" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Improved interface to the library problem ticket application.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Randall Library Staff
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="staff" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        An internal website used by library staff for various functions. The site houses a number of forms for
        initiating library processes. It is also used to gather input from library staff, especially when interviewing
        prospective library employees.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Randall Links
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="randall-links" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Panel for quick access to Randall Libray custom-written applications.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Record Number Lookup by Barcode
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="barcode-lookup" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Performed some automated maintenance of the library catalog.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Research Strategy
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="research-strategy" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        The library offers access to dozens of online databases, but using them for research can be a challenge. This
        application walks the student step-by-step through the process of creating an effective search statement to find
        relevant information in the databases.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Special Collections Patron Registration
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="special-collections-patron-registration" folder="randall" format="png"
          alt="application screen print" />
      </div>
      <div class="mb-6">
        This is used to gather information and create reports concerning patrons using the library's special collection
        and items contained within the collection.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Summon Deletes
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="summon-deletes" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Automated vendor notification of items removed from the library collection.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        TAC Circulation Transactions
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="tac-circulation-transactions" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        This application tracks student transactions at the university's Technology Assistance Center located within the
        library. The information is useful in helping management schedule the 30 plus part-time student workers to
        provide adequate coverage during all the hours the center is open.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        TAC Inventory Report
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="tac-inventory-report" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Helps keep track of the inventory of a wide variety of items in the library technical support department.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Ticket Tracking
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="ticket-tracking" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        This is an open source customer service ticket tracking application that I customized for use by the library.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Touch Kiosk
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="touch-kiosk" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Added book and video search to the library's handy public kiosk.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Transfers
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="transfers" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Manages the reporting and collection of unpaid patron late fees, lost items, and fines. When the amount due
        become too large, it gets transferred to the student's university account. At that point, the student cannot
        register for another semester without paying the fines.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Virtual Shelf
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="virtual-shelf" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        A tool for maintaining the library collection.
      </div>
    </div>

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Watchdog Reports
      </h3>
      <div class="w-full px-1 mb-6 mx-auto border border-warm_gray-200">
        <x-img name="watchdog" folder="randall" format="png" alt="application screen print" />
      </div>
      <div class="mb-6">
        Improved interface for working with the log files and error messages created by Drupal, the library's content
        management system.
      </div>
    </div>

  </div>

</x-layouts.app>
