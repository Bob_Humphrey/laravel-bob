@php
use App\Actions\GetPhpLinksAction;
$links = GetPhpLinksAction::execute();
@endphp

<x-layouts.app>

  <div class="text-lg w-full">

    <h2 class="w-full font-roboto_bold text-3xl text-black text-center pb-12 ">
      PHP
    </h2>

    @foreach ($links as $link)
      <div class="mb-10">
        <a href="{{ $link['url'] }}" class="block font-roboto_bold text-2xl text-gray-600 mb-2" target="_blank"
          rel="noopener noreferrer">
          {{ $link['title'] }}
        </a>

        @if (array_key_exists('text', $link))
          <div class="text-lg font-nunito_regular text-black text-justify">
            {!! $link['text'] !!}
          </div>
        @endif

      </div>
    @endforeach

  </div>

</x-layouts.app>
