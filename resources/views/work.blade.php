<x-layouts.app>

  <div class="text-lg">

    <h2 class="w-full font-roboto_bold text-3xl text-black text-center mb-3">
      Personal Projects
    </h2>

    <div class="text-center mb-10">
      <a href={{ url('/randall-projects') }} class="font-nunito_bold text-dark-tan hover:text-black">
        View Randall Library Projects here
      </a>
    </div>

    {{-- YAHTZEE --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Yahtzee
      </h3>

      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="yahtzee" folder="personal" format="jpg" alt="Yahtzee application screen print" />
      </div>

      <div class="mb-6 text-justify">
        I built this application to help teach myself how to use the
        Svelte web framework, which has scored very high (#1 in developer satisfaction) in a pair of recent surveys. It
        allows the
        user to play a quick game of Yahtzee against the computer. This seemed like a good subject for a learning
        project because of its relative simplicity and easy to capture rules. The logic used by the computer opponent is
        pretty basic, perhaps including about 80 percent
        of what is really needed to play a truly competitive game. I decided that 80 percent would be good enough for
        this app, because the
        objective of the project was to learn a new technology, and not to create the most intelligent computer
        adversary.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with JavaScript, Svelte and Tailwind.
      </div>
      <div class="flex">
        <a href="https://yahtzee.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/svelte-yahtzee" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- BLOG --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Blog
      </h3>

      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="blog1" folder="personal" format="jpg" alt="Scrapbook application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="blog4" folder="personal" format="jpg" alt="Scrapbook application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="blog2" folder="personal" format="jpg" alt="Scrapbook application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="blog5" folder="personal" format="jpg" alt="Scrapbook application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="blog3" folder="personal" format="jpg" alt="Scrapbook application screen print" />
      </div>

      <div class="mb-6 text-justify">
        In the year between the onset of the COVID-19 pandemic
        and the receiving of my vaccinations, I spent almost all of my time
        social distancing in a small apartment.
        At the end of this year, I was in serious need of something
        to pick up my spirits. During this time, I recalled an old
        story told by the spiritual writer Wayne Dyer, about how whenever he
        would spot a coin on the street, he would stop to pick it up and say a small prayer, thanking God for bringing
        this symbol of abundance into his life. "Never once," wrote Dyer, "have I asked, 'Why only a penny, God? You
        know I need a lot more than that.'" The story reminded me
        that even during the most restricted days of the pandemic,
        my life was still overflowing with so many symbols of abundance.
        I decided to create a small blog, and at least once a day add one these
        symbols of abundance to it, so that I would always have readily at
        hand a collection of some of the many things that I should be grateful for. The blog doesn't contain a lot of
        personal details from my life. Instead, it is mostly made up of things I enjoy that are
        available to anyone, especially art, music and writing.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://blog.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-blog" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- MY TRAVELING STAR --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        My Traveling Star
      </h3>

      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="star" folder="personal" format="jpg" alt="My Traveling Star application screen print" />
      </div>

      <div class="mb-6 text-justify">
        A collection of my travel photography. The title comes from a song by my favorite singer-songwriter, James
        Taylor.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://star.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-star" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- COVID-19 --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        COVID 19 Dashboard
      </h3>

      <div class="w-full mb-6 mx-auto border border-warm_gray-200">
        <x-img name="covid-19" folder="personal" format="png" alt="Covid 19 application screen print" />
      </div>

      <div class="mb-6 text-justify">
        A dashboard showing the number of people that contracted Covid-19, for the United States and each of its states
        and territories. The application uses information provided by
        <a href="https://covidtracking.com/" target="_blank" rel="noopener noreferrer"
          class="font-nunito_bold text-dark-tan hover:text-black">
          The COVID Tracking Project
        </a>,
        which collected data from the onset of the pandemic through May 7, 2021.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://covid19.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-covid19-php8" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- BLACKJACK --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Blackjack
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="blackjack" folder="personal" format="png" alt="Blackjack application screen print" />
      </div>

      <div class="mb-6 text-justify">
        This app shows what a good job the TALL Stack can do in building reactive displays. I had previously built this
        same application using React, and I believe the TALL Stack approach is equal or better in every way. Programming
        Blackjack is a fun challenge for the developer because the rules are completely straightforward and the options
        for the dealer are very limited.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://blackjack.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-blackjack" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- BOOKMARKS --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Bookmarks
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="bookmarks-1" folder="personal" format="png" alt="Bookmarks application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="bookmarks-2" folder="personal" format="png" alt="Bookmarks application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="bookmarks-3" folder="personal" format="png" alt="Bookmarks application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="bookmarks-4" folder="personal" format="png" alt="Bookmarks application screen print" />
      </div>

      <div class="mb-6 text-justify">
        Being a web developer means knowing a lot about a lot of different things. The field evolves extremely fast, and
        keeping up with the changes is a challenge. This bookmark manager is one of my best tools for dealing with this
        overabundance of information. I can click a button on my browser toolbar, and it will create a record using the
        current page title and url. I can then edit these items and add a category, organizing the articles I read for
        quick retrieval at a later date. I can also flag a website in one of two ways. First, to be read later, so that
        I always have a collection of articles that I want to read when more time is available. And second, as part of
        my 'learning list', that is, sites that I review on a regular basis to help keep up with new ideas and
        developments.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://bookmarks.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-bookmarks-version-2a" target="_blank"
          rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- SICBO --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Sicbo
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="sicbo" folder="personal" format="png" alt="Sicbo application screen print" />
      </div>

      <div class="mb-6 text-justify">
        This is another simple gambling game that shows the TALL Stack being used to create a reactive interface.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://sicbo.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-sicbo" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- TALL STACK PATTERNS --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Tall Stack Patterns
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="tall-stack" folder="personal" format="png" alt="Tall Stack Patterns application screen print" />
      </div>

      <div class="mb-6 text-justify">
        When I solve a problem as developer, I try to document the solution in some way so that it's quickly available
        the next time I need it. In this way, I'm not always reinventing the same old wheels. I put
        this application together to have this documentation in one place that I can easily refer to as I build new
        things.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://flex.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-tailwind" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- DOG BREED QUIZ --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Dog Breed Quiz
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="dog-breeds-1" folder="personal" format="png" alt="Dog Breed Quiz application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="dog-breeds-2" folder="personal" format="png" alt="Dog Breed Quiz application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="dog-breeds-3" folder="personal" format="png" alt="Dog Breed Quiz application screen print" />
      </div>

      <div class="mb-6 text-justify">
        I created this quiz to train myself to recognize many of the more common dog breeds. It uses the open source <a
          href="https://dog.ceo/dog-api/" class="font-nunito_bold text-dark-tan hover:text-black">
          Dog API
        </a>
        as a source of pictures and breed information.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the TALL Stack: Tailwind, Alpine, Laravel, and Livewire.
      </div>
      <div class="flex">
        <a href="https://dogs.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-dogs" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- KENO --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Keno
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="keno" folder="personal" format="png" alt="Keno application screen print" />
      </div>

      <div class="mb-6 text-justify">
        I have a cousin who, whenever she gets a little bit of money, heads to the nearest bar to gamble it away by
        playing Keno. This is a simple and popular game with absolutely terrible odds for the player. Even a short
        amount of time
        with my simulation shows how unlikely you are to ever win much of anything playing this game.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with React and Tailwind.
      </div>
      <div class="flex">
        <a href="https://keno.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/react-keno" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- LARAVEL SANCTUM --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Laravel Sanctum
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="sanctum-1" folder="personal" format="png" alt="Laravel Sanctum application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="sanctum-2" folder="personal" format="png" alt="Laravel Sanctum application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="sanctum-3" folder="personal" format="png" alt="Laravel Sanctum application screen print" />
      </div>

      <div class="mb-6 text-justify">
        I created this to demonstrate how to authenticate a React application using Laravel Sanctum. I posted a
        <a href="https://dev.to/dog_smile_factory/authenticating-a-react-app-with-laravel-sanctum-part-1-44hl"
          class="font-nunito_bold text-dark-tan hover:text-black">
          four part article describing the process on DEV.
        </a>
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the React, Laravel and Tailwind.
      </div>
      <div class="flex">
        <a href="https://auth.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/react-auth" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-auth" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- WEATHER FORECAST --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Weather Forecast
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="weather" folder="personal" format="png" alt="Weather Forecast application screen print" />
      </div>

      <div class="mb-6 text-justify">
        A weather forecasting application built using the
        <a href="https://darksky.net/dev" class="font-nunito_bold text-dark-tan hover:text-black">
          Dark Sky API
        </a>.
        Dark Sky has been acquired by Apple, and they have announced that the API will cease to exist at the end of
        2021.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with the React, Laravel and Tailwind.
      </div>
      <div class="flex">
        <a href="https://bookmarks.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/react-weather" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- INC 5000 --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Inc 5000
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="inc5000" folder="personal" format="png" alt="Inc 5000 application screen print" />
      </div>

      <div class="mb-6 text-justify">
        An app that demonstrates the sorting and filtering of a dataset, in this case, the
        <a href="https://www.inc.com/inc5000" class="font-nunito_bold text-dark-tan hover:text-black">
          Inc 5000 list
        </a>
        of fastest growing companies. The list can be filtered by rank,
        state, industry, name, revenue, growth, and other measures.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with React, Laravel and Tailwind.
      </div>
      <div class="flex">
        <a href="https://inc5000.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/react-inc5000" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/laravel-inc5000api" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- PERSONALITY QUIZ --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Personality Quiz
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="personality" folder="personal" format="png" alt="Personality Quiz application screen print" />
      </div>

      <div class="mb-6 text-justify">
        A personality quiz based on the
        <a href="https://openpsychometrics.org/tests/OJTS/development/"
          class="font-nunito_bold text-dark-tan hover:text-black">
          Open Extended Jungian Type Scales
        </a>,
        which is licenced under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
        Answering 32 questions will reveal your
        <a href="https://openpsychometrics.org/tests/OJTS/development/"
          class="font-nunito_bold text-dark-tan hover:text-black">
          Myers-Briggs personality type
        </a>.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with React and Tailwind.
      </div>
      <div class="flex">
        <a href="https://personality.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/react-personality" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>

    {{-- COLOR TOOL --}}

    <div class="bg-white mb-16 py-12 px-20 border border-warm_gray-200">
      <h3 class="font-roboto_bold text-2xl text-center mb-6">
        Color Tool
      </h3>

      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="color-1" folder="personal" format="png" alt="Color Tool application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="color-2" folder="personal" format="png" alt="Color Tool application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="color-3" folder="personal" format="png" alt="Color Tool application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="color-4" folder="personal" format="png" alt="Color Tool application screen print" />
      </div>
      <div class="w-full mb-6 mx-auto p-10 border border-warm_gray-200">
        <x-img name="color-5" folder="personal" format="png" alt="Color Tool application screen print" />
      </div>

      <div class="mb-6 text-justify">
        A tool for getting a quick idea of how a color scheme will look on the web. You pick a primary and secondary
        color from a color chart, and then several example websites are shown using the colors you've chosen.
      </div>
      <div class="font-nunito_italic mb-6">
        Built with Gatsby and React.
      </div>
      <div class="flex">
        <a href="https://color.bob-humphrey.com/" target="_blank" rel="noopener noreferrer">
          <x-heroicon-o-eye class="h-8 w-8 text-dark-tan hover:text-black mr-5" />
        </a>
        <a href="https://gitlab.com/Bob_Humphrey/gatsby-color" target="_blank" rel="noopener noreferrer">
          <x-fab-gitlab class="h-8 w-8 text-dark-tan hover:text-black" />
        </a>
      </div>
    </div>


</x-layouts.app>
