<x-layouts.app>

  {{-- LEFT COLUMN

  <div class="lg:w-2/5">
    <div class="hidden lg:block mb-0 filter grayscale hover:grayscale-0">
      <img src="{{ asset('img/dog1.png') }}" alt="Logo" />
    </div>
  </div> --}}

  {{-- RIGHT COLUMN --}}

  <div class="lg:w-3/5 text-lg font-nunito_regular leading-normal text-justify mx-auto">

    <div class="mb-4">
      <span class="font-roboto_bold text-2xl text-warm-gray-600">
        Hi, my name is Bob.
      </span>
      I'm a software developer, and I make websites that are fast, secure, and easy to use.
    </div>

    <div class="mb-4">
      I have many years of experience writing all kinds of code, but I whole heartedly reject the stereotype of
      the older developer who is behind the times and set in his ways. I am a lifelong learner who enjoys participating
      in the exciting advances being made in web development.
    </div>

    <div class="mb-4">
      My interests include walking, reading, photography, drawing and travel, and around the web I use the handle
      dog_smile_factory.
    </div>

  </div>

</x-layouts.app>
