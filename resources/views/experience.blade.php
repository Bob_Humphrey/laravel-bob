<x-layouts.app>

  <div class="text-lg">

    <h2 class="w-full font-roboto_bold text-3xl text-black text-center pb-6">
      Experience
    </h2>

    <div class="flex flex-col lg:flex-row pt-9 pb-3">

      <div class="w-full">

        <h3 class="font-roboto_bold text-2xl mb-6">
          University of North Carolina Wilmington (2011-2017)
        </h3>

        <div class=" text-justify leading-normal pb-6">
          <p class="mb-4">
            For 6 years I was the Web and Applications Developer for
            <a class="font-nunito_bold text-dark-tan hover:text-black" href="https://library.uncw.edu/" target="_blank"
              rel="noopener noreferrer">
              Randall Library
            </a>
            at
            <a class="font-nunito_bold text-dark-tan hover:text-black" href="https://uncw.edu/" target="_blank"
              rel="noopener noreferrer">
              The University of North Carolina Wilmington
            </a>.
            This is a busy academic library that sees around 7,000 students
            come through its doors every day, with several thousand
            more visiting its website.
          </p>
          <p>
            I was the library's only coder, and I was responsible for the full
            stack -- front end, back end, and servers. I completed
            <a class="font-nunito_bold text-dark-tan hover:text-black" href={{ url('randall-projects') }}>
              dozens of projects
            </a>
            using a
            <a class="font-nunito_bold text-dark-tan hover:text-black" href={{ url('skills') }}>
              wide variety of technologies
            </a>
            that improved workflows, cut costs, and helped the university's
            students, faculty and employees make better use of their library.
          </p>
        </div>


        <div class="flex flex-row flex-wrap -mx-3">
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L1" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L2" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L3" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L4" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L5" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L6" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L7" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L8" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L9" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L10" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L11" folder="experience" format="jpg" alt="Randall Library" />
          </div>
          <div class="w-1/2 lg:w-1/3 px-1 mb-2">
            <x-img name="L12" folder="experience" format="jpg" alt="Randall Library" />
          </div>
        </div>
      </div>
    </div>

    <div>

      <h3 class="font-roboto_bold text-2xl mt-12 mb-6">
        Roadway Express (1983-2007)
      </h3>

      <div class=" text-justify leading-normal pb-6">
        <p class="mb-4">
          During the years I worked at Roadway Express, it was one of the
          largest trucking companies in the country, with over 30,000 employees
          and more than 600 terminals located coast to coast. Today it is part
          of the trucking giant
          <a class="font-nunito_bold text-dark-tan hover:text-black" href="http://yrc.com/" target="_blank"
            rel="noopener noreferrer">
            YRC Freight
          </a>
          .
        </p>
        <p>
          Some of my titles at Roadway included Business Systems Analyst and
          Technical Team Lead. I was responsible for determining business
          requirements and putting together technical direction for a group of
          about twelve developers, building a variety of applications for the
          company's thousand member national sales force.
        </p>
      </div>


      <div class="flex flex-row flex-wrap -mx-3">
        <div class="w-1/2 lg:w-1/3 px-1 mb-2">
          <x-img name="Rex1" folder="experience" format="jpg" alt="Randall Library" />
        </div>
        <div class="w-1/2 lg:w-1/3 px-1 mb-2">
          <x-img name="Rex2" folder="experience" format="jpg" alt="Randall Library" />
        </div>
        <div class="w-1/2 lg:w-1/3 px-1 mb-2">
          <x-img name="Rex3" folder="experience" format="jpg" alt="Randall Library" />
        </div>
      </div>
    </div>

  </div>

</x-layouts.app>
