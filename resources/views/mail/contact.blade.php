<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bob Humphrey - Web Developer - Contact Message</title>
</head>

<body>

  <h1>
    Contact Message
  </h1>

  <h2>
    Name
  </h2>

  <div>
    {{ $name }}
  </div>

  <h2>
    Email
  </h2>

  <div>
    {{ $email }}
  </div>

  <h2>
    Message
  </h2>

  <div>
    {{ $content }}
  </div>

</body>

</html>
