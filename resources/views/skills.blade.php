<x-layouts.app>

  <div class="text-lg">

    <h2 class="w-full font-roboto_bold text-3xl text-black text-center pb-12">
      Skills
    </h2>

    <div class="font-roboto_bold text-2xl text-gray-600 text-center">
      <div>Laravel</div>
      <div>Tailwind CSS</div>
      <div>Livewire</div>
      <div>Alpine JS</div>
      <div>Inertia JS</div>
      <div>Svelte</div>
    </div>

    <div class="text-center my-12">
      I have experience using all of the tools below.
    </div>


    <div class="grid lg:grid-cols-3 text-gray-700 font-nunito_regular">
      <div class="">
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none pb-6">
          JavaScript
        </h3>
        <ul class="">
          <li class="">Livewire</li>
          <li class="">Alpine</li>
          <li class="">Svelte</li>
          <li class="">Inertia</li>
          <li class="">Gatsby</li>
          <li class="">React</li>
          <li class="">Node / npm</li>
          <li class="">Vue</li>
          <li class="">jQuery</li>
          <li class="">Express</li>
        </ul>
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none py-6">
          PHP
        </h3>
        <ul class="">
          <li class="">Laravel</li>
          <li class="">Yii</li>
          <li class="">Drupal coding</li>
          <li class="">Wordpress coding</li>
        </ul>
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none py-6">
          Database
        </h3>
        <ul class="">
          <li class="">SQL</li>
          <li class="">GraphQL</li>
          <li class="">MySQL</li>
          <li class="">phpMyAdmin</li>
          <li class="">PostgreSQL</li>
          <li class="">Microsoft Access</li>
        </ul>
      </div>

      <div class="pt-6 lg:pt-0">
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none pb-6">
          Front End
        </h3>
        <ul class="">
          <li class="">HTML</li>
          <li class="">CSS</li>
          <li class="">Tailwind CSS</li>
          <li class="">Bootstrap</li>
          <li class="">Zurb Foundation</li>
          <li class="">Bulma</li>
        </ul>
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none py-6">
          Static Sites
        </h3>
        <ul class="">
          <li class="">Eleventy</li>
          <li class="">Gatsby</li>
          <li class="">Jigsaw</li>
          <li class="">Hugo</li>
          <li class="">Jekyll</li>
        </ul>
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none py-6">
          Content Mgt
        </h3>
        <ul class="">
          <li class="">Directus</li>
          <li class="">Cockpit</li>
          <li class="">Drupal</li>
          <li class="">WordPress</li>
          <li class="">Joomla</li>
        </ul>
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none py-6">
          Graphics
        </h3>
        <ul class="">
          <li class="">Affinity Designer</li>
          <li class="">Affinity Photo</li>
          <li class="">Photoshop</li>
          <li class="">Illustrator</li>
          <li class="">Canon Digital Photo Professional</li>
          <li class="">Riot - Radical Image Optimization Tool</li>
        </ul>
      </div>

      <div class="pt-6 lg:pt-0">
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none pb-6">
          DevOps
        </h3>
        <ul class="">
          <li class="">Windows</li>
          <li class="">Linux</li>
          <li class="">Ubuntu</li>
          <li class="">Red Hat</li>
          <li class="">CentOS</li>
          <li class="">Digital Ocean</li>
          <li class="">Linode</li>
          <li class="">PuTTY</li>
          <li class="">FileZilla</li>
        </ul>
        <h3 class="text-2xl font-roboto_bold text-blue-500 leading-none py-6">
          Misc
        </h3>
        <ul class="Misc">
          <li class="">Gitlab</li>
          <li class="">Github</li>
          <li class="">Visual Studio</li>
          <li class="">Firefox Web Development Tools</li>
          <li class="">Markdown</li>
          <li class="">Postwoman</li>
          <li class="">Postman</li>
          <li class="">Lighthouse</li>
          <li class="">Bash</li>
        </ul>
      </div>
    </div>

    <div>

</x-layouts.app>
