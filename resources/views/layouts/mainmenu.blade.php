<a href="{{ url('/experience') }}" class="hover:text-dark-tan lg:px-12 py-2">
  Experience
</a>
<a href="{{ url('/work') }}" class="hover:text-dark-tan lg:px-12 py-2">
  Personal Projects
</a>
<a href="{{ url('/randall-projects') }}" class="hover:text-dark-tan lg:px-12 py-2">
  Professional Projects
</a>
<a href="{{ url('/skills') }}" class="hover:text-dark-tan lg:px-12 py-2">
  Skills
</a>
<a href="{{ url('/php') }}" class="hover:text-dark-tan lg:px-12 py-2">
  PHP
</a>
<a href="{{ url('/contact') }}" class="hover:text-dark-tan lg:px-12 py-2">
  Contact
</a>
