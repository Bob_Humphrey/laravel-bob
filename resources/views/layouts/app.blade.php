<x-layouts.base>
  <div class="font-nunito_regular text-warm-gray-600 bg-gray-50">

    {{-- @include('layouts.screen-width') --}}

    <nav class="bg-white border-b border-warm_gray-200 py-3" x-data="{ showMenu: false }">

      {{-- XL SCREEN --}}

      <div class="hidden xl:flex flex-row justify-center text-base font-inter_light text-dark-gray leading-none py-3">
        @include('layouts.mainmenu')
      </div>

      <div class="hidden xl:flex w-full justify-center pt-2">
        <div class="w-16">
          <a href="https://bob-humphrey.com">
            <img src="/img/dog-1.png" alt="Bob Humphrey Web Development">
          </a>
        </div>
      </div>

      <div class="hidden xl:flex justify-center pt-1 pb-6">
        <a href="/" class="">
          <h1 class="font-roboto_bold text-4xl text-black hover:text-dark-tan">
            {{ config('app.name', 'Laravel Application') }}
          </h1>
        </a>
      </div>

      {{-- SMALLER SCREENS --}}

      <div class="flex xl:hidden justify-around items-center">

        <div class="xl:hidden flex justify-center mt-0 mb-2">
          <div class="w-16">
            <a href="https://bob-humphrey.com">
              <img src="/img/dog-1.png" alt="Bob Humphrey Web Development">
            </a>
          </div>
        </div>

        <div class="flex xl:justify-start"> <a href="/" class="flex flex-col xl:flex-row w-full">
            <h1 class="font-roboto_bold text-3xl text-black hover:text-dark-tan">
              {{ config('app.name', 'Laravel Application') }}
            </h1>
          </a>
        </div>

        <div class="xl:hidden w-8 cursor-pointer hover:text-dark-tan" @click="showMenu=!showMenu" x-show="!showMenu">
          <x-zondicon-menu />
        </div>
        <div class="xl:hidden w-8 cursor-pointer hover:text-dark-tan" @click="showMenu=!showMenu" x-show="showMenu">
          <x-zondicon-close />
        </div>
      </div>

      <div
        class="flex flex-col xl:hidden text-lg font-inter_light text-black hover:text-dark-tan leading-none py-4 px-10"
        x-show="showMenu">
        @include('layouts.mainmenu')
      </div>

    </nav>

    @include('layouts.flash')

    <main class="lg:flex w-11/12 sm:w-10/12 md:w-7/12 xl:w-6/12 mx-auto py-12 px-6">

      {{ $slot }}

    </main>

    @include('layouts.footer')
  </div>
</x-layouts.base>
