<?php

namespace App\Actions;

class GetPhpLinksAction
{
  public static function execute()
  {
    $links = [
      [
        'url' => 'https://phpnews.io/',
        'title' => 'PHP News'
      ],
      [
        'url' => 'https://laravel-news.com/',
        'title' => 'Laravel News'
      ],
      [
        'url' => 'https://blog.jetbrains.com/phpstorm/2021/11/the-php-foundation/',
        'title' => 'The New Life of PHP – The PHP Foundation',
        'text' => "The PHP Foundation will be a non-profit organization whose mission is to ensure the long life and prosperity of
        the PHP language."
      ],
      [
        'url' => 'https://neilpatel.com/blog/simple-php-framework/',
        'title' => 'Why A Simple PHP Framework Works for Websites',
        'text' => "Reasons to Use a Simple PHP Framework for Websites: (1) Simplified Coding, (2) Quicker Development, (3)
        Increased Security, (4) Easier Maintenance, (5) Improved Teamwork."
      ],
      [
        'url' => 'https://arstechnica.com/gadgets/2021/09/php-maintains-an-enormous-lead-in-server-side-programming-languages/',
        'title' => 'PHP maintains an enormous lead in server-side programming languages',
        'text' => "The venerable web programming language PHP is a source of frequent complaints and frustration, but according to
        a report W3Techs released today, it doesn't seem to be going away any time soon. W3Techs' web server survey looks for technologies in use by sites in Alexa's top 10 million list; today's report
        includes a year-on-year chart beginning with January 2010, running all the way through 2021. Within that dataset, the story told is clear. Apart from PHP—which held a 72.5 percent share in 2010 and holds a
        78.9 percent share as of today—only one other server-side language ever broke a 10 percent share. That one
        competitor is ASP.NET, which held an impressive 24.4 percent share in 2010 but was down to 9.3 percent in
        January and 8.3 percent this month."
      ],
      [
        'url' => 'https://bulletproofphp.dev/yes-php-is-worth-using',
        'title' => 'Yes, PHP is Worth Learning/Using in $CURRENT_YEAR',
        'text' => "It's absolutely possible to write reliable, clean, maintainable code in PHP that you and/or your team will be
        happy with. Not only that, but there are parts of PHP that you might even prefer to how things work in other
        languages you're familiar with."
      ],
      [
        'url' => 'https://www.jetbrains.com/lp/php-25/',
        'title' => 'PHP Turns 25'
      ],
      [
        'url' => 'https://www.jesuisundev.com/en/why-developers-hate-php/',
        'title' => 'Why developers hate PHP',
        'text' => "You’ve been hearing for 10 years that PHP is going to die. Yet he’s still here. Despite time and the latest
        hypothetical technology, it’s not moving. Developers hate PHP because it is the opposite of hype driven
        development. In a profession where everyone invests quickly on the latest stuff, PHP is an old man who doesn’t
        want to sell his land."
      ],
      [
        'url' => 'https://christoph-rumpel.com/2020/8/refactoring-php',
        'title' => 'Refactoring PHP',
        'text' => "I've been programming in PHP now for almost ten years, and if there is one thing I learned over this period,
        it's that readability and simplicity are the keys for maintainable and sustainable code. Every first attempt to
        write code should be about making it work. Only after that, you should take some time to refactor. This is when
        I aim for readability and simplicity. Today I see refactoring as one of my main skills. In this post, I share
        with you my refactoring practices for PHP."
      ],
      [
        'url' => 'https://github.com/jupeter/clean-code-php',
        'title' => 'Clean Code in PHP',
        'text' => "Software engineering principles, from Robert C. Martin's book <i>Clean Code</i>, adapted for PHP. This is not a
          style guide. It's a guide to producing readable, reusable, and refactorable software in PHP. Not every principle
          herein has to be strictly followed, and even fewer will be universally agreed upon. These
          are guidelines and nothing more, but they are ones codified over many years of collective experience by the
          authors of <i>Clean Code</i>."
      ],
      [
        'url' => 'https://phptherightway.com/',
        'title' => 'PHP The Right Way'
      ],
    ];

    return $links;
  }
}
