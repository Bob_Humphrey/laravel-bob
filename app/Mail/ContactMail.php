<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */

  public $name;
  public $email;
  public $content;

  public function __construct($form)
  {
    $this->name = $form['name'];
    $this->email = $form['email'];
    $this->content = $form['content'];
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->view('mail.contact')
      ->subject('Web Developer Contact Form');
  }
}
